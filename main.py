#!/usr/bin/env python3

import sys
import urllib.parse as urlparse
import mysql.connector
import string
import random
import time
import os
from http.server import HTTPServer, BaseHTTPRequestHandler

mydb = mysql.connector.connect(
  host="web.gtech",
  user="URLShortener",
  passwd="yMeSueCsXqaRvjoz",
  database="UrlShortner"
)
sqlQuery = mydb.cursor()

# Used for making "short" URLS
validCharacters= string.ascii_letters+'-_1234567890'

class Redirect(BaseHTTPRequestHandler):

    def do_GET(self):
        location = (urlparse.urlparse(self.path).path)
        API.headersOK(self,200)
      # If api is called...
        if(location[1:4] == 'api'):
            print('[API] Got an API call...')
           # If the URL Api is called...
            if(location[5:9] == 'urls'):
                print('[API:URL] got a call...')
               # If the URL Api Find method is called...
                if(location[10:14] == 'find'):
                    urlToFind = (location[15:len(location)])
                    try:
                        newLocation = API.findURL(self,urlToFind)
                    except IndexError:
                        newLocation = "https://gt3ch1.tk/urls"
                    print("[API:URL:find] Done. Redirecting.")
                    API.writeHTML(self,"<script>window.location = '"+newLocation+"';</script>")
                elif(location[10:14] == 'make'):
                    urlToMake = (location[15:len(location)])
                    shortenedURL = API.makeURL(self,urlToMake)
                    newLocation = API.findURL(self,shortenedURL)
                    if(len(shortenedURL) < 1):
                        success = "false"
                    else:
                        success = "true"
                    arr = [["success", success],["short", shortenedURL]]
                    response = API.createJSON(self,arr)
                    print(response)
                    return
                else:
                    API.error(self,404)
            elif(location[5:7] == 'dl'):
                print('[API:DL] got a call...')
                if(location[8:13] == 'music'):
                    print('[API:DL:MUSIC] got a call...')
                    print(location[14:len(location)])
                    command = os.system("ssh maintenance@file01-serv.pease.net -C 'bash ~/scripts/download_music -u https://www.youtube.com/watch?v="+location[14:len(location)]+"'")
                    print('[API:DL:MUSIC] ' + str(command))
                    if(command):
                        success = "false"
                    else:
                        success = "true"
                    arr = [["success", success]]
                    response = API.createJSON(self,arr)
                    print("[API:DL:MUSIC] -- " + response)
                    return
                else:
                    API.error(self,404)
            else:
                API.error(self,404)
        else:
            API.error(self,404)

class API():
    def createJSON(self,array):
        response = '{'
        for row in array:
            for elem in row:
                if(row.index(elem) % 2):
                    response += '"'+elem+'",'
                else:
                    response += '"'+elem+'":'
        response = response[0:len(response)-1]
        response += '}'


        self.wfile.write(str.encode(response))
        return response
    def writeHTML(self,html):
        self.wfile.write(str.encode(html))
    def error(self,err):
        html ='Error: '+str(err)
        self.send_response(err)
        API.writeHTML(self,html)
    def headersOK(self,response):
        self.send_response(response)
        self.send_header('Access-Control-Allow-Origin','*')
        self.send_header('Content-type','text/html')
        self.end_headers()
    def findURL(self,knownshort):

        print('===[API:URL:findURL]')
        print("[API:URL:findURL] - knownshort: "+knownshort)
        sqlQuery.execute("SELECT * FROM URLs WHERE `short` ='"+str(knownshort)+"'")
        sqlResult = sqlQuery.fetchmany(1)
        newLocation = (str(sqlResult).split("'")[1])
        if("http" not in newLocation):
            newLocation = "http://"+newLocation
        if(len(newLocation) < 9):
            newLocation = "https://gt3ch1.tk/urls"
        print("[API:URL:findURL] found location: " + newLocation)

        return newLocation


    def makeURL(self, urlToMake):
        print("===[API:URL:make]")
        print("[API:URL:make] - urlToMake: "+urlToMake)
        if(("'" in urlToMake) or ("%3E" in urlToMake) or ("%3c" in urlToMake)):
            print("Zoinks!")
            self.send_response(418)
        else:
            randomString = ''.join(random.choice(validCharacters) for i in range(10))
            sql = "INSERT INTO URLs (url, short) VALUES (%s, %s)"
            val = (urlToMake, randomString)
            sqlQuery.execute(sql, val)
            mydb.commit()
            print("[API::make] - new shortened URL at : " + randomString)
            return randomString

HTTPServer(("", 8080), Redirect).serve_forever()
